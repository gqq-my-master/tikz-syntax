grammar Tikz;


COMMENT: '%' .*? NEWLINE;

ID : [a-zA-Z]+ ; // match identifiers


UNIT: 'pt'
    | 'pc'
    | 'in'
    | 'bp'
    | 'cm'
    | 'mm'
    | 'dd'
    | 'cc'
    | 'sp';

DIMENSION: NUMBER UNIT?;

NUMBER: '.' [0-9]+
    | [0-9]+ ('.' [0-9]+)?;



NEWLINE:'\r'? '\n' ; // return newlines to parser (is end-statement signal) 20
WS : [ \t]+; // -> skip ; // toss out whitespace

name: ID (WS ID)*;

absoluteCoordinate: '(' DIMENSION ',' DIMENSION  ')' // xy coordinate
   | '(' DIMENSION ',' DIMENSION ',' DIMENSION  ')' // xyz coordinate
   | '(' NUMBER ':' DIMENSION  ')' // polar coordinate
   ;


coordinate : absoluteCoordinate
    | '(' name ('.' name)? ')' //anchor
    ;

relativeMoveCoordinate: '++' absoluteCoordinate;

relativeStayCoordinate: '+' absoluteCoordinate;

relativeCoordinate: relativeMoveCoordinate
    | relativeStayCoordinate
    ;


operation: '--'
    | 'rectangle'
    ;

path:
    coordinate WS? operation
    (WS? (relativeCoordinate|coordinate) WS? operation)*
    WS? 'cycle'?
    // cycle only works with at leat two -- operations, e.g.
    // (3,1)  -- (3,3) -- cycle
    ;

pathCommandName: '\\path'
    | '\\draw'
    | '\\fill'
    | '\\filldraw'
    | '\\clip'
    ;

option: '[' ']';

command: pathCommandName WS? option? WS? path WS? ';';


